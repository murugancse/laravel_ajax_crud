<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $dept = Department::find(1);
        $users = $dept->users()->paginate(3);
        //$users = Department::with('users')->find(1)->paginate(3);
        //$users = Department::find(1)->users->paginate(3);;
        //return $users;
        return view('home',compact('users'));
    }

    public function getNext(Request $request)
    {
        //$dept = Department::find(1);
        //$users = $dept->users()->paginate(3);
        $data = array();
        $data['status'] = 0; 
        $data['is_last'] = 0;
        $next = User::where('id', '>', $request->last_id)->where('department_id', '=', 1)->orderBy('id')->first();
        if(!empty($next)){
            $data['status'] = 1; 
            $data['data'] = $next; 
            $data['is_last'] =  count(User::where('id', '>', $request->last_id)->where('department_id', '=', 1)->get()) >1 ? 1 : 0;
        }
        return $data;
    }
    public function saveUser(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $status = $user->save();
        if($status){
            $data = array( 'status' => 1, 'message' => 'Saved successfully');
        }else{
            $data = array( 'status' => 0, 'message' => 'Failed to save');
        }
        return $data;
    }
    public function deleteUser(Request $request)
    {
        $res=User::where('id',$request->userid)->delete();
        if($res){
            $data = array( 'status' => 1, 'message' => 'Deletion successfully');
        }else{
            $data = array( 'status' => 0, 'message' => 'Failed to delete');
        }
        return $data;
    }
}
