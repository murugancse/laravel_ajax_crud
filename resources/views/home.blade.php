@extends('layouts.crud')

@section('headSection')
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cyborg/bootstrap.min.css" rel="stylesheet" type="text/css">
<style>
    body{
        background-color: #8c6868;
    }
    

    .dropdown-item {
        display: block;
        width: 100%;
        padding: .25rem 1.5rem;
        clear: both;
        font-weight: 400;
        color: #fff;
        text-align: inherit;
        white-space: nowrap;
        /* background-color: transparent; */
        border: 0;
    }
    navbar-nav {
        float: right;
        margin: 0;
    }
</style>
@endsection

@section('content')
<div class="container">
    
    <table id="usertable" class="table table-bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Addmore</th>
          </tr>
        </thead>
        <tbody>
        @php
        $i = 1
        @endphp
         @foreach ($users as $user)
            <tr data-id="{{ $user->id }}">
                <td>{{ $user->name }} </td>
                <td>{{ trim($user->email) }} </td>
                <td>
                    @if ($i==3)
                        <button id="AddNext" type="button" class="btn btn-sm btn-default" onclick="AddNext({{ $user->id }},this);"><span class="glyphicon glyphicon-plus"> </span></button>
                    @endif
                </td>
                @php
                $i++;
                @endphp
            </tr>
         @endforeach 
         
        </tbody>
      </table>
</div>
@endsection

@section('footerSection')
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{ asset('js/bootstable.js') }}"></script>
<script>
  $(document).ready(function() {
    $('table').SetEditable();
  });
  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  function AddNext(lastid,tddata){
      $("#AddNext").attr('disabled',true);

        var addbutton='';
        jQuery.ajax({
            url: "{{ url('/getNext') }}",
            method: 'post',
            data: {
                last_id: lastid,
            },
            success: function(resultdata){
                if(resultdata.status==1){
                    var result = resultdata.data;
                    if(resultdata.is_last==1){
                        addbutton = '<button id="AddNext" type="button" class="btn btn-sm btn-default" onclick="AddNext('+result.id+',this);"><span class="glyphicon glyphicon-plus"> </span></button>';
                    }
                    $('#usertable > tbody:last-child').append('<tr data-id="'+result.id+'"><td>'+result.name+'</td><td>'+result.email+'</td><td>'+addbutton+'</td><td name="buttons">'+newColHtml+'</td></tr>');
                    $(tddata).remove();
                }else{
                    console.log(resultdata);
                }
                
            }
        });
    }

    function UpdateUsers(userdata){
        jQuery.ajax({
            url: "{{ url('/saveUser') }}",
            method: 'post',
            data: userdata,
            success: function(resultdata){
                if(resultdata.status==1){
                    alert(resultdata.message);
                }else{
                    console.log(resultdata);
                }
                
            }
        });
    }
    function removeRow(dataid){
        jQuery.ajax({
            url: "{{ url('/deleteUser') }}",
            method: 'post',
            data: { userid : dataid},
            success: function(resultdata){
                if(resultdata.status==1){
                    alert(resultdata.message);
                }else{
                    console.log(resultdata);
                }
                
            }
        });
    }
</script>
@endsection